const DB_NAMES = {
    node: 'node-api'
};

const PRODUCTS_MESSAGES = {
    ADD_PRODUCT: 'Product created successfully',
    GET_PRODUCTS: 'Products data fetched successfully'
}

module.exports = { DB_NAMES, PRODUCTS_MESSAGES };